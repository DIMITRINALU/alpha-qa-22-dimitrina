package pages;

import com.youtube.youtube.Utils;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class YouTubePage extends BasePage {

    private WebDriver driver;
    public YouTubePage(WebDriver driver) {
        super(driver);
    }

    public void searchAndPlayMusicInYouTube(String searchSong) {
        actions.waitForElementVisible("youtube.Search", 150);
        actions.assertElementPresent("youtube.Search");
        actions.typeValueInField(searchSong, "youtube.Search");
        actions.waitForElementVisible("youtube.SearchSong", 50);
        actions.assertElementPresent("youtube.SearchSong");
        actions.clickElement("youtube.SearchSong");
        actions.waitForElementVisible("youtube.Text", 50);
        actions.assertElementPresent("youtube.Text");
        actions.clickElement("youtube.Text");
        actions.waitForElementVisible("youtube.Text", 50);
        Utils.getWebDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        actions.clickElement("youtube.pause");
    }
}
