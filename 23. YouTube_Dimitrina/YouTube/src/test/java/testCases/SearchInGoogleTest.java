package testCases;

import com.youtube.youtube.Utils;
import org.junit.Test;
import pages.GooglePage;

import java.util.concurrent.TimeUnit;

public class SearchInGoogleTest extends BaseTest {

    String searchCriterion = "YouTube";

    @Test
    public void simpleGoogleSearch() {
        GooglePage google = new GooglePage(actions.getDriver());
        google.SearchAndOpenFirstResult(searchCriterion);
        signInToYouTube();
        actions.assertNavigatedUrl("youtube.url");
    }

    private void signInToYouTube() {
        Utils.getWebDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        actions.assertElementPresent("youtube.SignInToYouTube");
        actions.clickElement("youtube.SignInToYouTube");
    }
}