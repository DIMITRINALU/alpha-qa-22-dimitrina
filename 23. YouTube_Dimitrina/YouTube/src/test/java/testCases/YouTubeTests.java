package testCases;

import com.youtube.youtube.Utils;
import org.junit.Test;
import pages.GooglePage;
import pages.YouTubePage;

import java.util.concurrent.TimeUnit;

public class YouTubeTests extends BaseTest {

    String searchCriterion = "YouTube";
    String searchSong = "johnny cash man comes around";

    @Test
    public void playVideo_When_YouTubePageLoaded() {

        GooglePage google = new GooglePage(actions.getDriver());
        google.SearchAndOpenFirstResult(searchCriterion);
        signInToYouTube();

        YouTubePage playMusic = new YouTubePage(actions.getDriver());
        playMusic.searchAndPlayMusicInYouTube(searchSong);
        Utils.getWebDriver().manage().window().fullscreen();
    }

    private void signInToYouTube() {
        Utils.getWebDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        actions.assertElementPresent("youtube.SignInToYouTube");
        actions.clickElement("youtube.SignInToYouTube");
    }
}