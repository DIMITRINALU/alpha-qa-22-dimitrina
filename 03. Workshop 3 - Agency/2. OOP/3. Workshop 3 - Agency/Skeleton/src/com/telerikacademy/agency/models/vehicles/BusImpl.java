package com.telerikacademy.agency.models.vehicles;

import static com.telerikacademy.agency.models.common.VehicleType.LAND;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    private static final int PASSENGER_MIN_CAPACITY = 10;
    private static final int PASSENGER_MAX_CAPACITY = 50;
    private static final String BUS_CAPACITY = "A bus cannot have less than 10 passengers or more than 50 passengers";

    private int passengerCapacity;

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, LAND);
    }

    @Override
    protected void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < PASSENGER_MIN_CAPACITY || passengerCapacity > PASSENGER_MAX_CAPACITY) {
            throw new IllegalArgumentException(BUS_CAPACITY);
        }
        this.passengerCapacity = passengerCapacity;
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    protected void setPricePerKilometer(double pricePerKilometer) {
        super.setPricePerKilometer(pricePerKilometer);
    }

    @Override
    public double getPricePerKilometer() {
        return super.getPricePerKilometer();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}