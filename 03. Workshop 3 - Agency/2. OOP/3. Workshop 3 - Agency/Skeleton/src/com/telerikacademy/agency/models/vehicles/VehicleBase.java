package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleBase implements Vehicle {

    private static final String TYPE_CANT_BE_NULL = "Type can`t be null";
    private static final String CAPACITY = "Capacity cannot be less than 1 and more than 800!";
    private static final String PRICE_MSG = "A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!";
    private static final int PASSENGER_MIN_CAPACITY = 1;
    private static final int PASSENGER_MAX_CAPACITY = 800;
    private static final double MIN_PRICE = 0.10;
    private static final double MAX_PRICE = 2.50;

    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;

    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        setType(type);
    }

    protected void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < PASSENGER_MIN_CAPACITY || passengerCapacity > PASSENGER_MAX_CAPACITY) {
            throw new IllegalArgumentException(CAPACITY);
        }
        this.passengerCapacity = passengerCapacity;
    }

    protected void setPricePerKilometer(double pricePerKilometer) {
        if (pricePerKilometer < MIN_PRICE || pricePerKilometer > MAX_PRICE) {
            throw new IllegalArgumentException(PRICE_MSG);
        }
        this.pricePerKilometer = pricePerKilometer;
    }

    private void setType(VehicleType type) {
        if (type == null)
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        this.type = type;
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    public String printClassName() {
        return getClass().getSimpleName().replaceAll("Impl","");
    }

    @Override
    public String print() {
        return String.format("%s ----%n" +
                "Passenger capacity: %d%n" +
                "Price per kilometer: %.2f%n" +
                "Vehicle type: %s%n", printClassName(), getPassengerCapacity(), getPricePerKilometer(), getType());
    }

    @Override
    public String toString() {
        return print();
    }
}