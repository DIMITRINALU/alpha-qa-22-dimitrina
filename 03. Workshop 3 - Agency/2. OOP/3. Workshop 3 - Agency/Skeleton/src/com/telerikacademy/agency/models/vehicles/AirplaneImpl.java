package com.telerikacademy.agency.models.vehicles;

import static com.telerikacademy.agency.models.common.VehicleType.AIR;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {

    private boolean hasFreeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer, AIR);
        setHasFreeFood(hasFreeFood);
    }

    private void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    protected void setPassengerCapacity(int passengerCapacity) {
        super.setPassengerCapacity(passengerCapacity);
    }

    @Override
    protected void setPricePerKilometer(double pricePerKilometer) {
        super.setPricePerKilometer(pricePerKilometer);
    }

    @Override
    public int getPassengerCapacity() {
        return super.getPassengerCapacity();
    }

    @Override
    public double getPricePerKilometer() {
        return super.getPricePerKilometer();
    }

    @Override
    public boolean hasFreeFood() {
        return hasFreeFood;
    }

    @Override
    public String print() {
        return super.print() + String.format("Has free food: %b%n", hasFreeFood);
    }
}