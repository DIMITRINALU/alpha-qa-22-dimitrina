package com.telerikacademy.agency.models.vehicles;

import static com.telerikacademy.agency.models.common.VehicleType.LAND;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    private static final int PASSENGER_MIN_CAPACITY = 30;
    private static final int PASSENGER_MAX_CAPACITY = 150;
    private static final String TRAIN_CAPACITY = "A train cannot have less than 30 passengers or more than 150 passengers";
    private static final String CARTS_QUANTITY = "A train cannot have less than 1 cart or more than 15 carts";
    private static final int CART_MIN_QUANTITY = 1;
    private static final int CART_MAX_QUANTITY = 15;

    private int passengerCapacity;
    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, LAND);
        setCarts(carts);
    }

    private void setCarts(int carts) {

        if (carts < CART_MIN_QUANTITY || carts > CART_MAX_QUANTITY ) {
            throw new IllegalArgumentException(CARTS_QUANTITY);
        }
        this.carts = carts;
    }

    public int getCarts() {
        return carts;
    }

    @Override
    protected void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < PASSENGER_MIN_CAPACITY || passengerCapacity > PASSENGER_MAX_CAPACITY) {
            throw new IllegalArgumentException(TRAIN_CAPACITY);
        }
        this.passengerCapacity = passengerCapacity;
    }

    @Override
    protected void setPricePerKilometer(double pricePerKilometer) {
        super.setPricePerKilometer(pricePerKilometer);
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return super.getPricePerKilometer();
    }

    @Override
    public String print() {
        return super.print() + String.format("Carts amount: %d%n", getCarts());
    }
}