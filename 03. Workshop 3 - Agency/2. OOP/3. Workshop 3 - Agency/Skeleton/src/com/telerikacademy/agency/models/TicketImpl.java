package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {

    private Journey journey;
    private double administrativeCosts;

    public TicketImpl(Journey journey, double administrativeCosts) {
        setJourney(journey);
        setAdministrativeCosts(administrativeCosts);
    }

    private void setJourney(Journey journey) {
        this.journey = journey;
    }

    private void setAdministrativeCosts(double administrativeCosts) {
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    @Override
    public double calculatePrice() {
        return journey.calculateTravelCosts() * administrativeCosts;
    }

    @Override
    public String print() {
        return String.format("Ticket ----%n" +
                "Destination: %s%nPrice: %.2f%n", journey.getDestination(),calculatePrice());
    }

    @Override
    public String toString() {
        return print();
    }
}