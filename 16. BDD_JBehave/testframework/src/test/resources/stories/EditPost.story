Meta:
@editPost

Narrative:
As a registered user
I want to edit topic

Scenario: 003 Edit Topic
When Wait forum.HomePage element 2000 milliseconds
Then Click forum.HomePage element
When Wait forum.EditReply element 2000 milliseconds
Then Click forum.EditReply element
When Wait forum.EditPost element 2000 milliseconds
And Click forum.EditPost element
When Wait forum.TypeHereTextArea element 1000 milliseconds
And Click forum.TypeHereTextArea element
And Type . Just great! in forum.TypeHereTextArea field
Then Click forum.SaveEdit element