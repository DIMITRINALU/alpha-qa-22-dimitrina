package stepdefinitions;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StepDefinitions extends BaseStepDefinitions {
    UserActions actions = new UserActions();

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void click(String element) {
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void type(String value, String field) {
        actions.typeValueInField(value, field);
    }

    @Given("Wait $element element $seconds milliseconds")
    @When("Wait $element element $seconds milliseconds")
    @Then("Wait $element element $seconds milliseconds")
    public void waitForElement(String element, int milliseconds) {
        actions.waitForElementVisible(element, milliseconds);
    }

    @Given("Element $locator is present")
    @When("Element $locator is present")
    @Then("Element $locator is present")
    public void elementPresent(String locator) {
        actions.assertElementPresent(locator);
    }
}