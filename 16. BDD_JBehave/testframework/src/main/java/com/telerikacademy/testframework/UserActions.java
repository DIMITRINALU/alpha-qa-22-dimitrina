package com.telerikacademy.testframework;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class UserActions {

    private static final String TELERIK_URL = "https://stage-forum.telerikacademy.com/";

    private static WebDriver driver;
    private static WebElement webElement;

    public UserActions() {
        driver = Utils.getWebDriver();
    }

    public static void loadBrowser() {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey("forum.url"));
    }

    public static void logInForum() {
        
        Assert.assertEquals(driver.getCurrentUrl(), TELERIK_URL);
        driver.findElement(By.className("d-button-label")).click();
        driver.findElement(By.id("Email")).sendKeys("dimitrinamurtova@gmail.com");
        driver.findElement(By.id("Password")).sendKeys("test");
        driver.findElement(By.id("next")).click();

        try {
            webElement = driver.findElement(By.className("d-button-label"));
        } catch (Exception e) {
            Assert.assertNotNull(webElement);
        }
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    public void clickElement(String key) {
        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        element.click();
    }

    public void typeValueInField(String value, String field) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        element.sendKeys(value);
    }

    //############# WAITS #########

    public void waitForElementVisible(String locator, int miliseconds) {
        try {
            Thread.sleep(miliseconds);
        } catch (Exception exception) {
            Assert.fail("Element with locator: '" + locator + "' was not found.");
        }
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }
}