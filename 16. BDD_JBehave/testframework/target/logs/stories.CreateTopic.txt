
(stories/CreateTopic.story)
Meta:
@topicCreate 

Scenario: 001 Create Topic in Telerik Academy stage forum
Given Click forum.NewTopicButton element
When Element forum.AddTitleToTopic is present
And Click forum.AddTitleToTopic element
And Type amazingTest in forum.AddTitleToTopic field
And Type testing forever in forum.TypeHereTextArea field
Then Click forum.CreateTopic element


