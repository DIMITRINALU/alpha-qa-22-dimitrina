Meta:
@topicReply

Narrative:
As a registered user
I want to comment on topic
In order to discuss JBehave with my colleagues

Scenario: 002 Reply on Topic
When Wait forum.HomePage element 2000 milliseconds
Then Click forum.HomePage element
When Wait forum.PostReply element 2000 milliseconds
Then Click forum.PostReply element
When Wait forum.Reply element 2000 milliseconds
And Click forum.Reply element
And Click forum.ReplyTextArea element
And Type Great! in forum.ReplyTextArea field
Then Click forum.LastReply element


