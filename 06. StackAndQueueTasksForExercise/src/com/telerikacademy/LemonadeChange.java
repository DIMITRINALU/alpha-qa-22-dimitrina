package com.telerikacademy;

import java.util.*;

public class LemonadeChange {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int[] bills = Arrays
                .stream(sc.nextLine().split(","))
                .mapToInt(Integer::parseInt).toArray();

        lemonadeChange(bills);
    }

    private static void lemonadeChange(int[] bills) {

        int five = 0;
        int ten = 0;

        boolean isTrue = true;

        for (int bill : bills) {

            if (bill == 5) {
                five++;
            } else if (bill == 10) {
                if (five == 0) {
                    isTrue = false;
                    System.out.println("false");
                    break;
                }
                five--;
                ten++;
            } else {
                if (five > 0 && ten > 0) {
                    five--;
                    ten--;
                } else if (five >= 3) {
                    five -= 3;
                } else {
                    isTrue = false;
                    System.out.println("false");
                    break;
                }
            }
        }

        if (isTrue) {
            System.out.println("true");
        }
    }
}