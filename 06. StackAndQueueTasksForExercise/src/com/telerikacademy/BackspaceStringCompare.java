package com.telerikacademy;
import java.util.Scanner;
import java.util.Stack;

public class BackspaceStringCompare {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String S = sc.nextLine();
        String T = sc.nextLine();

        backspaceCompare(S, T);
    }

    private static void backspaceCompare(String S, String T) {
        System.out.println(build(S).equals(build(T)));
    }

    private static Stack build(String S) {

        Stack<Character> result = new Stack();

        for (char letter: S.toCharArray()) {

            if (letter != '#')
                result.push(letter);
            else if (!result.empty())
                result.pop();
        }
        return result;
    }
}