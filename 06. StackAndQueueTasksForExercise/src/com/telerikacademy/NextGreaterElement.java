package com.telerikacademy;
import java.util.*;

public class NextGreaterElement {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int[] first = Arrays
                .stream(sc.nextLine().split(","))
                .mapToInt(e -> Integer.parseInt(e)).toArray();

        int[] second = Arrays
                .stream(sc.nextLine().split(","))
                .mapToInt(e -> Integer.parseInt(e)).toArray();

        nextGreaterElement(first, second);
    }

    private static void nextGreaterElement(int[] first, int[] second) {

        Map<Integer, Integer> hashMap = new HashMap<>();
        Stack<Integer> stack = new Stack<>();

        for (int number : second) {

            while (!stack.empty() && stack.peek() < number) {
                hashMap.put(stack.pop(), number);
            }
            stack.push(number);
        }

        int[] result = new int[first.length];

        for (int i = 0; i < first.length; i++) {
            result[i] = hashMap.getOrDefault(first[i], -1);
        }

        System.out.println(Arrays.toString(result));
    }
}