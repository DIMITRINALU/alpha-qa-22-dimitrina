package com.telerikacademy;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;
import java.util.stream.Collectors;

public class BaseballGame {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        String elements = sc.nextLine();
        List<String> strings = Arrays.stream(elements.split(","))
                .collect(Collectors.toList());

        collectPoints(strings);
    }

    private static void collectPoints(List<String> strings) {
        Stack<Integer> stack = new Stack();

        for (String str : strings) {

            switch (str) {
                case "+":
                    int currentRound = stack.pop();
                    int previousRound = currentRound + stack.peek();
                    stack.push(currentRound);
                    stack.push(previousRound);
                    break;
                case "C":
                    stack.pop();
                    break;
                case "D":
                    stack.push(2 * stack.peek());
                    break;
                default:
                    stack.push(Integer.valueOf(str));
                    break;
            }
        }

        int finalPoints = 0;
        for (int points : stack) {
            finalPoints += points;
        }

        System.out.println(finalPoints);
    }
}