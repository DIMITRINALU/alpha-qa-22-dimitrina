package com.telerikacademy;
import java.util.Scanner;
import java.util.Stack;

public class ValidParentheses {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String parentheses = sc.nextLine();

        validParentheses(parentheses);
    }

    private static void validParentheses(String parentheses) {

        if (parentheses.length() % 2 == 1) {
            System.out.println("false");
            return;
        }

        Stack<Character> openingBrackets = new Stack<>();

        for (int i = 0; i < parentheses.length(); i++) {

            char current = parentheses.charAt(i);

            if (current == '(' || current == '[' || current == '{') {
                openingBrackets.push(current);
            }
            else {

                char peekChar = openingBrackets.peek();

                if ((current == ')' && peekChar != '(')
                        || (current == '}' && peekChar != '{')
                        || (current == ']' && peekChar != '[')) {

                    System.out.println("false");
                    return;

                } else {
                    openingBrackets.pop();
                }
            }
        }

        System.out.println("true");
    }
}