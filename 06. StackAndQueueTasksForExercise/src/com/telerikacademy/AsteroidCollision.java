package com.telerikacademy;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class AsteroidCollision {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int[] asteroids = Arrays
                .stream(sc.nextLine().split(", "))
                .mapToInt(Integer::parseInt).toArray();

        asteroidCollision(asteroids);
    }

    private static void asteroidCollision(int[] asteroids) {

        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < asteroids.length; i++) {
            if (stack.isEmpty()) {
                stack.push(asteroids[i]);
            }
            else if (asteroids[i] > 0 && stack.peek() > 0 || stack.peek() < 0 && asteroids[i] < 0
                    || stack.peek() < 0 && asteroids[i] > 0) {
                stack.push(asteroids[i]);
            }
            else if (Math.abs(stack.peek()) < Math.abs(asteroids[i])) {
                stack.pop();
                --i;
            }
            else if (Math.abs(stack.peek()) > Math.abs(asteroids[i])) {
                continue;
            }
            else {
                stack.pop();
            }
        }

        int size = stack.size();
        int[] result = new int[size];

        while (size-- > 0) {
            result[size] = stack.pop();
        }
        System.out.println(Arrays.toString(result));
    }
}