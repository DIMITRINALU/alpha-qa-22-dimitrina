package com.telerikacademy;

import java.util.Stack;

public class ImplementQueueUsingStack {

    private Stack<Integer> stackOne = new Stack<>();
    private Stack<Integer> stackTwo = new Stack<>();

    /** Initialize your data structure here. */
    public ImplementQueueUsingStack() {

    }

    /** Push element x to the back of queue. */
    public void push(int x) {
        stackOne.push(x);
    }

    /** Removes the element from in front of queue and returns that element. */
    public int pop() {
        if (stackTwo.isEmpty()) {
            while (!stackOne.isEmpty())
                stackTwo.push(stackOne.pop());
        }
        return stackTwo.pop();
    }

    /** Get the front element. */
    public int peek() {
        if (!stackTwo.isEmpty()) {
            return stackTwo.peek();
        } else {
            while (!stackOne.isEmpty())
                stackTwo.push(stackOne.pop());
        }
        return stackTwo.peek();
    }

    /** Returns whether the queue is empty. */
    public boolean empty() {
        return stackOne.isEmpty() && stackTwo.isEmpty();
    }
}