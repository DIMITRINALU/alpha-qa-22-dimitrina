package com.telerikacademy;
import java.util.*;

public class SetMismatch {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int[] numbers = Arrays
                .stream(sc.nextLine().split(","))
                .mapToInt(Integer::parseInt).toArray();

        findErrorNumbers(numbers);
    }

    private static void findErrorNumbers(int[] numbers) {

        HashSet<Integer> set = new HashSet<>();
        int[] result = new int[2];

        int arrSum = (1 + numbers.length) * numbers.length / 2;
        int sum = 0;

        for (int num : numbers) {
            if (set.contains(num))
                result[0] = num;
            else {
                set.add(num);
                sum += num;
            }
        }

        result[1] = arrSum - sum;

        System.out.println(Arrays.toString(result));
    }
}