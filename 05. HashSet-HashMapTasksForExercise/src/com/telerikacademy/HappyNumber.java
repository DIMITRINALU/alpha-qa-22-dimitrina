package com.telerikacademy;

import java.util.HashSet;
import java.util.Scanner;

public class HappyNumber {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();

        isHappy(number);
    }

    public static void isHappy(int number) {

        HashSet<Integer> set = new HashSet<>();
        boolean isHappy = false;

        while (set.add(number)) {

            int sum = 0;
            int remain = 0;

            while (number > 0) {
                remain = number % 10;
                sum += remain * remain;
                number /= 10;
            }

            if (sum == 1) {
                isHappy = true;
            } else {
                number = sum;
            }
        }

        if (isHappy) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}