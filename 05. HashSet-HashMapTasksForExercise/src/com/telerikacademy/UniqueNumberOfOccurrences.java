package com.telerikacademy;

import java.util.*;

public class UniqueNumberOfOccurrences {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int[] numbersArr = Arrays
                .stream(sc.nextLine().split(","))
                .mapToInt(Integer::parseInt).toArray();

        uniqueOccurrences(numbersArr);

    }

    private static void uniqueOccurrences(int[] numbersArr) {

        HashMap<Integer, Integer> counter = new HashMap<>();
        boolean isUnique = false;

        for (int num : numbersArr) {
            counter.compute(num, (k, v) -> v == null ? 1 : v + 1);
        }

        Set<Integer> uniqueOccurrences = new HashSet<>();

        for (Integer value : counter.values()) {
            uniqueOccurrences.add(value);
        }

        if (uniqueOccurrences.size() == counter.size()) {
            isUnique = true;
        }

        if (isUnique) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}