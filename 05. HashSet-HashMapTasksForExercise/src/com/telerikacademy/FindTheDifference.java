package com.telerikacademy;

import java.util.Scanner;

public class FindTheDifference {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        String t = sc.nextLine();

        findTheDifference(s, t);
    }

    private static void findTheDifference(String s, String t) {

        char result = 0;

        for (char item : s.toCharArray()) {
            result ^= item;
        }

        for (char item : t.toCharArray()) {
            result ^= item;
        }

        System.out.println(result);
    }
}