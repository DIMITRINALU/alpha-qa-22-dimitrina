package com.telerikacademy;
import java.util.Scanner;

public class JewelsAndStones {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String J = sc.nextLine();
        String S = sc.nextLine();

        numberJewelsInStones(J, S);
    }

    private static void numberJewelsInStones(String J, String S) {

        int result  = 0;

        for(int i = 0; i < S.length(); i++){
            for(int j = 0; j < J.length(); j++){
                if(J.charAt(j) == S.charAt(i)){
                    result ++;
                    break;
                }
            }
        }

        System.out.println(result);;
    }
}