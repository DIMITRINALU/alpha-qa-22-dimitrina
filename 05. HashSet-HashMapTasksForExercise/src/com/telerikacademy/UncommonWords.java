package com.telerikacademy;

import java.util.*;

public class UncommonWords {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String A = sc.nextLine();
        String B = sc.nextLine();

        uncommonWords(A, B);
    }

    private static void uncommonWords(String A, String B) {

        Map<String, Integer> count = new HashMap();

        for (String word : A.split(" ")) {
            count.put(word, count.getOrDefault(word, 0) + 1);
        }

        for (String word : B.split(" ")) {
            count.put(word, count.getOrDefault(word, 0) + 1);
        }

        List<String> result = new LinkedList();

        for (String word : count.keySet()) {
            if (count.get(word) == 1) {
                result.add(word);
            }
        }
        System.out.println(String.join(",", result));
    }
}