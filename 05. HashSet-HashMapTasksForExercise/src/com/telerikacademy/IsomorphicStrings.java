package com.telerikacademy;

import java.util.HashMap;
import java.util.Scanner;

public class IsomorphicStrings {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        String t = sc.nextLine();

        isIsomorphic(s, t);
    }

    private static void isIsomorphic(String s, String t) {

        boolean isIsomorphic = true;

        if (s.length() != t.length()) {
            isIsomorphic = false;
        }

        HashMap<Character, Character> first = new HashMap<>();
        HashMap<Character, Character> second = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {
            char ch1 = s.charAt(i);
            char ch2 = t.charAt(i);

            if (first.containsKey(ch1)) {
                if (ch2 != first.get(ch1)) {
                    isIsomorphic = false;
                }
            } else {
                if (second.containsKey(ch2)) {
                    isIsomorphic = false;
                }

                first.put(ch1, ch2);
                second.put(ch2, ch1);
            }
        }

        if (isIsomorphic) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}