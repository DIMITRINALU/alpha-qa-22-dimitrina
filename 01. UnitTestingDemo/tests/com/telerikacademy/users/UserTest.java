package com.telerikacademy.users;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class UserTest {

    private static class UserTestClass extends User {

        public UserTestClass(String username) {
            super(username);
        }
    }

    @Test(expected = NullPointerException.class)
    public void User_should_throw_when_usernameIsNull() {
        new User(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUser_shouldThrow_when_usernameIsBellow_USERNAME_MIN_LENGTH() {
        User user = new User("");
        String username = user.getUsername();
        assertEquals(user.getUsername().length(),username.length());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUser_shouldThrow_when_usernameIsAbove_USERNAME_MAX_LENGTH() {
        User user = new User("DidiDidiDidiDidiDidiDidi");
        String username = user.getUsername();
        assertEquals(user.getUsername().length(),username.length());
    }

    @Test
    public void getUsername_should_return_validName() {
        User user = new User("Didi");
        String username = user.getUsername();
        assertEquals(user.getUsername(), username);
    }
}