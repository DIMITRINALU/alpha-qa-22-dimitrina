import com.telerikacademy.MyArrayList;
import com.telerikacademy.MyList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyArrayListTests {

    private MyList<Integer> list;

    @Before
    public void init() {

        this.list = new MyArrayList<>();
    }

    @Test
    public void creatingListShouldHaveZeroCount() {

        assertEquals(0, this.list.getUsedPositions());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getOnEmptyListShouldThrowExceptionWithZeroAsIndex() {
        this.list.get(0);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getOnEmptyListShouldThrowExceptionWithNegativeAsIndex() {
        this.list.get(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getOnEmptyListShouldThrowExceptionWithPositiveAndGreaterThanZeroAsIndex() {
        this.list.get(1);
    }

    @Test
    public void testIndexOfShouldReturnMinusOneForEmptyList() {

        assertEquals(-1, list.findIndexOf(1));
    }

    @Test
    public void testIndexOfShouldReturnCorrectIndex() {

        for (int i = 0; i < 100; i++) {
            list.add(i);
        }

        assertEquals(73, list.findIndexOf(73));
    }

    @Test
    public void addShouldIncreaseCount() {

        list.add(1);
        assertEquals(1, this.list.getUsedPositions());
    }

    @Test
    public void addShouldIncreaseSize() {

        for (int i = 0; i < 100; i++) {
            list.add(i);
        }

        assertEquals(100, this.list.getUsedPositions());
    }

    @Test
    public void testSizeShouldReturnZeroEmptyList() {

        Assert.assertEquals(0, list.getUsedPositions());
        list.add(1);
        Assert.assertEquals(1, list.getUsedPositions());
    }

    @Test
    public void testSizeShouldReturnOneHundred() {

        for (int i = 0; i < 100; i++) {
            list.add(i);
        }

        assertEquals(100, list.getUsedPositions());
    }

    @Test
    public void testGetShouldReturnTheCorrectElement() {

        for (int i = 0; i < 100; i++) {
            list.add(i);
        }
        assertEquals(99, list.get(99));
    }

    @Test
    public void testSetShouldChangeTheElement() {

        for (int i = 0; i < 100; i++) {
            list.add(i);
        }

        list.set(99, 666);
        assertEquals(666, list.get(99));
    }

    @Test
    public void addAndGetMultipleElements() {

        for (int i = 0; i < 100; i++) {
            list.add(i);
        }

        for (Integer i = 0; i < 100; i++) {
            assertEquals(i, list.get(i));
        }
    }

    @Test
    public void testGetFirstShouldReturnButNotRemove() {

        for (int i = 0; i < 100; i++) {
            list.add(i);
        }

        assertEquals(0, list.getFirst());
        assertEquals(0, list.getFirst());
        assertEquals(100, list.getUsedPositions());
    }

    @Test
    public void testGetLastShouldReturnButNotRemove() {

        for (int i = 0; i < 100; i++) {
            list.add(i);
        }
        assertEquals(99, list.getLast());
        assertEquals(99, list.getLast());
        Assert.assertEquals(100, list.getUsedPositions());
    }

    @Test
    public void testSize() {

        assertEquals(4, this.list.getSize());
        list = new MyArrayList<>(new Integer[100]);
        assertEquals(100, this.list.getSize());
    }

    @Test
    public void testIterator() {

        MyList<String> strings = new MyArrayList<>();

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 100; i++) {
            strings.add(String.valueOf(i));
            sb.append(i);
        }

        String values = sb.toString();

        Iterator<String> iter = strings.iterator();
        assertNotNull(iter);
        int counter = 0;
        for (iter = strings.iterator(); iter.hasNext(); ) {
            if (values.contains(iter.next())) {
                counter++;
            } else {
                counter = -1;
                break;
            }
        }
        assertEquals(strings.getUsedPositions(), counter);
    }
}