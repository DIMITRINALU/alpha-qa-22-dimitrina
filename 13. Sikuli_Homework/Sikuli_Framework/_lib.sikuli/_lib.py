from sikuli import *
import unittest
import iHTMLTestRunner.iHTMLTestRunner as HTMLTestRunner
from _uimap import *

class TelerikForum:
	@classmethod
	def StartWIN(self):			
		type('r', Key.WIN)
		find("search.png").highlight(2)
		type('chrome /incognito')
		type(Key.ENTER)	
		sleep(5)
		
	
	@classmethod
	def CloseWIN(self):
		type(Key.ALT, Key.F4)
