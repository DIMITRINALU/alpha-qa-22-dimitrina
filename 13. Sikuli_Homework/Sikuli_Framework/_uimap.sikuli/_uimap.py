########################################################
# UI map for XYZ
########################################################
from sikuli import *
########################################################

class TelerikForumUI:
    TextField = "TextField.png"
    Search = "search.png"
    LogIn = "Log.png"
    User = "user.png"
    User_icon = "user_icon.png"
    LogOut = "LogOut.png" 
    NewTopic = "NewTopic.png"
    CreateTopic = "CreateTopic.png"
    TextArea = "TextArea.png" 
    HomePage = "HomePage.png" 
    UncategorizedLink = "UncategorizedLink.png"
    WhyNotCreateAtopic = Region(83,497,566,37)
    SearchTopic = "searchTopic.png"
    ChooseTopic = "Testfornewto.png"
    Edit = "edit.png"
    SaveEdit = "saveEdit.png"
    Reply = "Reply.png"
    ReplyTopic = "replytopic.png"
    ChooseTopictToReplay = "Testfornewto-2.png"
    React = "react.png"
    Points = "points.png"
    DeleteTopic = "deleteBtn.png"
    ChooseTopicToDelete = "Testfornewto-2.png"
