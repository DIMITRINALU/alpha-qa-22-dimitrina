from _lib import *

    
class SmokeTests(unittest.TestCase):
       
    def test_001_OpenWindows(self):
        TelerikForum.StartWIN()        
    
    def test_002_Input_Link(self):
        wait(TelerikForumUI.TextField)       
        type('https://stage-forum.telerikacademy.com/')
        type(Key.ENTER)
        sleep(3)        
 
    def test_003_LogIn_Forum(self):      
        click("Log.png")
        sleep(5)
        type('dimitrinamurtova@gmail.com')
        type(Key.TAB)
        type('test')
        type(Key.ENTER)
        sleep(5)

    def test_004_Create_New_Topic_From_Home_Page(self):      
        click(TelerikForumUI.NewTopic)
        sleep(5)
        type('Test for new topic creation from home page')
        click(TelerikForumUI.TextArea)
        type('I am so excited about SikuliX')
        click(TelerikForumUI.CreateTopic)
        sleep(5)

    def test_005_Create_New_Topic_From_WhyNotCeateATopic_In_The_Footer_Of_A_Category(self):      
        click(TelerikForumUI.HomePage)
        sleep(5)
        click(TelerikForumUI.UncategorizedLink)
        sleep(5)        
        click(TelerikForumUI.WhyNotCreateAtopic)
        sleep(5)
        type('Test for topic creation from footerRegion')
        click(TelerikForumUI.TextArea)
        type('The topic is all we need')
        click(TelerikForumUI.CreateTopic)
        sleep(5)

    def test_006_Edit_Post(self):      
        click(TelerikForumUI.HomePage)
        sleep(5)
        click(TelerikForumUI.UncategorizedLink)
        sleep(10)
        click(TelerikForumUI.SearchTopic)
        type('Test for new topic creation from home page')
        click(TelerikForumUI.ChooseTopic)
        sleep(5)
        click(TelerikForumUI.Edit)
        type('Test title change')
        click(TelerikForumUI.TextArea)
        type('I am so excited about SikuliX...oooo yeah')
        click(TelerikForumUI.SaveEdit)
        sleep(3)

    def test_007_Post_A_Reply(self): 
        click(TelerikForumUI.HomePage)
        sleep(5)
        click(TelerikForumUI.UncategorizedLink)
        sleep(7)
        click(TelerikForumUI.SearchTopic)
        type('Test for new topic creation from home page title change')
        click(TelerikForumUI.ChooseTopictToReplay)
        sleep(3)
        click(TelerikForumUI.Reply)
        type('OKI')
        sleep(1)
        click(TelerikForumUI.ReplyTopic)
        sleep(3)

    def test_008_React_To_A_Post(self): 
        click(TelerikForumUI.HomePage)
        sleep(5)
        click(TelerikForumUI.UncategorizedLink)
        sleep(10)
        click(TelerikForumUI.SearchTopic)
        type('Test for new topic creation from home page title change')
        click(TelerikForumUI.ChooseTopictToReplay)
        sleep(3)
        click(TelerikForumUI.React)
        sleep(2)

    def test_009_Remove_Topic(self):
        click(TelerikForumUI.HomePage)
        sleep(5)
        click(TelerikForumUI.UncategorizedLink)
        sleep(10)
        click(TelerikForumUI.SearchTopic)
        type('Test for new topic creation from home page title change')
        click(TelerikForumUI.ChooseTopicToDelete)
        sleep(5)
        click(TelerikForumUI.Points)
        click(TelerikForumUI.DeleteTopic)
        sleep(3)

    def test_010_LogOut_Forum(self):
        click(TelerikForumUI.User)
        sleep(3)
        click(TelerikForumUI.User_icon)
        wait(1)
        click(TelerikForumUI.LogOut)
        sleep(3)        

    def test_11_CloseWindows(self):
        TelerikForum.CloseWIN()
        

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SmokeTests)
    
    print("WARNING: You may need to adapt UI map to match your windows specifics")
    outfile = open("Report.html", "w")
    runner = HTMLTestRunner.HTMLTestRunner(stream=outfile, title='SmokeTests Report' )
    runner.run(suite)
    outfile.close()

