CREATE DATABASE Minions

USE Minions

CREATE TABLE Minions(
	Id INT PRIMARY KEY NOT NULL,
	[Name] NVARCHAR(50) NOT NULL,
	Age TINYINT
)

CREATE TABLE Towns(
	Id INT PRIMARY KEY NOT NULL,
	[Name] NVARCHAR(50) NOT NULL
)

ALTER TABLE Minions
ADD TownId INT FOREIGN KEY REFERENCES Towns(Id)

INSERT INTO Towns(Id, [Name])
	VALUES
		(1, 'Sofia'),
		(2, 'Plovdiv'),
		(3, 'Varna'),
		(4, 'Burgas'),
		(5, 'Popovo')

SELECT * FROM Towns

INSERT INTO Minions (Id, [Name], Age, TownId)
	VALUES
		(1, 'Didi', 16, 1),
		(2, 'Alexa',22, 4),
		(3, 'Emilio', NULL, 3),
		(4, 'Todorova', 24, 5),
		(5, 'Stefaka', 23, 2)

SELECT * from Minions

SELECT * FROM Towns

TRUNCATE TABLE Minions

SELECT * FROM Minions

DROP TABLE Minions

DROP TABLE Towns
