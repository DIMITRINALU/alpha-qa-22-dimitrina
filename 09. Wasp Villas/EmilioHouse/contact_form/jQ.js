 let added = 0;
 function thankYou() {
    const $thank =$("#thank");
    $thank.css("font-size","25px");
    $thank.append(`Thank you! We will contact you as soon as possible!`);
 }
 function add() {
    const $list =$('#guestList');
    
    const $name = $('#name');
    const $email = $('#email');
    const $adate = $('#arrDate');
    const $ddate = $('#depDate');
    const $adults = $('#adults');
    const $children = $('#children');
    
    if ($adate.val() !== null && $adate.val() !== '' && 
    $ddate.val() !== null && $ddate.val() !== '' &&
    $adults.val() !== null && $adults.val() !== '' &&
    $email.val() !== null && $email.val() !== '' &&
    $name.val() !== null && $name.val() !== '') {
    
    const $todo = $(`<p> Name: ${$name.val()} <br> 
    E-mail: ${$email.val()} <br>
    Arrival Date: ${$adate.val()}<br>
    Departure date ${$ddate.val()}<br>
    Adults: ${$adults.val()}<br>
    Children: ${$children.val()} </p>`);

    $list.append($todo);

    const log = { name: $name.val(), 
    email: $email.val(), 
    arrival:$adate.val(), 
    departure: $ddate.val(), 
    adults: $adults.val(),
    children: $children.val()};
    
    fetch('https://reqres.in/api/users' , {
        method: 'POST',
        headers: {'Content-type': 'application/json'
    },
    body: JSON.stringify(log)
    }).then(res => {return res.json()})
    .then(data => console.log(data))
       
    $todo.val("");
    added = 1;
}
else {alert('Please fill all required fields')}
};
 

$(document).ready(function() {
    $("#submit").click(function() {
       if (added===1) {$("#guestList").hide(); thankYou(); added=0; }
       else { alert ("You have to add a request first") }        

    });
});

