# Homework: Create test cases covering the following functionality of a forum: 	
* Comments	
* Creation of topics 	
	
Tests Designed by: Dimitrina	
Tests Designed date: 20.08.2020	

URL to test: https://forum.telerikacademy.com/

## TC1
Test Title: Accessing the system using {webBrowser}

Narrative:
1.I want to use Telerik Academy Forum	

Steps to reproduce:
1. Open web browser 
2. Loading URL address: https://forum.telerikacademy.com/

Priority: 1
-----------------

## TC2
Test Title: Login with correct username and password

Narrative:
1. In order to use Forum
2. As a future student of Telerik Academy
3. I want to Log In Telerik Academy Forum	

Steps to reproduce:
1. Loading URL address: https://forum.telerikacademy.com/
2. Click on "Log In" button
3. Enter valid username
4. Enter valid password
5. Click on "Sign In" button

Priority: 1
------------------

## TC3
Test Title: Login with correct username and wrong password

Narrative:
1. In order to use Forum
2. As a future student of Telerik Academy
3. I want to Log In Telerik Academy Forum

Steps to reproduce:
1. Loading URL address: https://forum.telerikacademy.com/
2. Click on "Log In" button
3. Enter valid username
4. Enter invalid password
5. Click on "Sign In" button

Priority: 1
-------------------

## TC4
Test Title: Login with wrong username and correct password

Narrative:
1. In order to use Forum
2. As a future student of Telerik Academy
3. I want to Log In Telerik Academy Forum

Steps to reproduce:
1. Loading URL address: https://forum.telerikacademy.com/
2. Click on "Log In" button
3. Enter invalid username
4. Enter valid password
5. Click on "Sign In" button

Priority: 1
-----------------------

## TC5
Test Title: Login with Academy account and correct username and password

Narrative:
1. In order to use Forum
2. As a current student of Telerik Academy
3. I want to Log In Telerik Academy Forum

Steps to reproduce:
1. Loading URL address: https://forum.telerikacademy.com/
2. Click on "Log In" button
3. Enter valid username
4. Enter valid password
5. Click on "Sign In With Telerik Academy" button

Priority: 1
-----------------------

## TC6
Test Title: Login with Academy account and correct username and wrong password

Narrative:
1. In order to use Forum
2. As a current student of Telerik Academy
3. I want to Log In Telerik Academy Forum

Steps to reproduce:
1. Loading URL address: https://forum.telerikacademy.com/
2. Click on "Log In" button
3. Enter valid username
4. Enter invalid password
5. Click on "Sign In With Telerik Academy" button

Priority: 1
-------------------------

## TC7
Test Title: Login with Academy account and wrong username and correct password

Narrative:
1. In order to use Forum
2. As a current student of Telerik Academy
3. I want to Log In Telerik Academy Forum

Steps to reproduce:
1. Loading URL address: https://forum.telerikacademy.com/
2. Click on "Log In" button
3. Enter invalid username
4. Enter valid password
5. Click on "Sign In With Telerik Academy" button

Priority: 1
----------------------

## TC8
Test Title: Register with Sing Up Now hyperlink

Narrative:
1. In order to use Forum
2. As a student (current/future) of Telerik Academy
3. I want to Register for Telerik Academy Forum

Steps to reproduce:
1. Loading URL address: https://forum.telerikacademy.com/
2. Click on "Log In" button
3. Click on "Sing Up Now" hyperlink
4. Enter username
5. Enter password
6. Confirm password
7. Select "I aggree to the terms of use" checkbox
8. Click on "Register" button

Priority: 1
----------------------------

## TC9
Test Title: Create New Topic from Home Page

Narrative:
1. In order to create topic for discussion
2. As a student of Telerik Academy
3. I want to create a new topic, related to current program

Steps to reproduce:
1. Loading URL address: https://forum.telerikacademy.com/
2. Click on "New Topic" button
3. Enter title or paste link in label area
4. Enter message in text area
5. Select and Click on "Create Topic" button

Priority: 2
-------------------------------

## TC10
Test Title: Create New Topic from Alpha22QA Category Page

Narrative:
1. In order to create topic for discussion
2. As a student of Telerik Academy
3. I want to create a new topic, related to current modul

Steps to reproduce:
1. Loading URL address: https://forum.telerikacademy.com/
2. Click on "Categories" button from Navigation bar
3. Click on category name "Alpha 22 QA" hyperlink
4. Click on "New Topic" button
5. Enter title or paste link in label area
6. Enter message in text area
7. Select and Click on "Create Topic" button

Priority: 2
----------------------------

## TC11
Test Title: Create New Topic from footer topic list button from Alpha22QA Category Page

Narrative:
1. In order to create topic for discussion
2. As a student of Telerik Academy
3. I want to create a new topic, related to current modul

Steps to reproduce:
1. Loading URL address: https://forum.telerikacademy.com/
2. Click on "Categories" button from Navigation bar
3. Click on category name "Alpha 22 QA" hyperlink
4. Click on "Why not create a topic?" hyperlink
5. Enter title or paste link in label area
6. Enter message in text area
7. Select and Click on "Create Topic" button

Priority: 2
----------------------------------

## TC12
Test Title: Create New Topic from Topic Page

Narrative:
1. In order to create topic for discussion
2. As a student of Telerik Academy
3. I want to create a new topic, related to current project

Steps to reproduce:
1. Loading URL address: https://forum.telerikacademy.com/
2. Click on "Categories" button from Navigation bar
3. Click on category name "Alpha 22 QA" hyperlink
4. Click on topic "Group HTML Page Project" hyperlink
5. Click on "Home page" hyperlink
6. Click on "New Topic" button
7. Enter title or paste link in label area
8. Enter message in text area
9. Select and Click on"Create Topic" button

Priority: 2
----------------------------

## TC13

Test Title: Write Comment on Topic

Narrative:
1. In order to make comments on topics
2. As a student of Telerik Academy
3. I want to comment a topic, related to current project

Steps to reproduce:
1. Loading URL address: https://forum.telerikacademy.com/
2. Click on "Categories" button from Navigation bar
3. Click on category name "Alpha 22 QA" hyperlink
4. Click on topic "Group HTML Page Project" hyperlink
5. Choose topic to comment
6. Click on "Reply" button
7. Enter comment in text area
8. Select and Click on "Reply" button

Priority: 3