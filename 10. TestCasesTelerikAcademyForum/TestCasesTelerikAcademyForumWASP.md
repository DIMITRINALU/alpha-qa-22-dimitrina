# Homework: Create test cases covering the following functionality of a forum: 	
  * Comments	
  * Creation of topics 	
	
Tests Designed by: Buddy Group I	
Tests Designed date: 20.08.2020	

URL to test: https://forum.telerikacademy.com/


## TC1
* Test Title: Create new Topic from Home Page

* Precondition: The system is accessible via {webBrowser} and https://forum.telerikacademy.com/ loaded. User is logged in.

* Narrative: Аs a student of Telerik Academy I want to create a new, uncategorized topic.

* Steps to reproduce:
  1. Click on "New Topic" button
  2. Enter title or paste link in label area
  3. Enter message in text area
  4. Select and Click on "Create Topic" button 

Priority: Medium
-----------------

## TC2
* Test Title: Create new Topic from "Why not create a topic?" in the footer of a category

* Preconditions: 
  1. Access https://forum.telerikacademy.com/ via Google Chrome Web Browser
  2. Log into forum with valid credentials

* Narrative: As a registered user of the Telerik forum, check if a new topic can be created using the "Why not create a topic?" hyperlink located in the footer of each category on the forum.

* Steps to reproduce:
  1. Click on any category on forum main page
  2. Scroll down to bottom of page
  3. Click on "Why not create a topic?" hyperlink
  4. Enter topic title and topic body (mandatory fields)
  5. Click on "Create Topic" button below topic body OR press Ctrl + Enter

Priority: Low
------------------

## TC3
* Test Title: Create Topic as a registered user

* Precondition: The system is accessible via webBrowser and https://forum.telerikacademy.com/ loaded. User is logged in.

* Narrative: New topic with unique name will be created by registered user by uing the "+ New Topic" button in the upper rght corner of the Home screen

* Steps to reproduce:
  1. Click on "+ New Topic" button in the upper rght corner of the Home screen
  2. Enter unique topic title and topic body 
  3. Click on "Create Topic" button below topic body OR press Ctrl + Enter


Priority: High
-------------------

## TC4
* Test Title: Add category to new topic

* Precondition: Web browser Google Chrome(Version 85.0.4183.83) is opend and https://forum.telerikacademy.com/ loaded with registerd user.

* Narrative: Аs a student of Telerik Academy I want to create a new topic and add that topic to a specific category.

* Steps to reproduce:
  1. Click on "New Topic" button in the top right section of the page.
  2. Add some random text as a title(10 symbols) and body (5 words).
  3. Switch the drop down menu for category from "Uncategorized" to "Alpha Preparation".
  4. Select and Click on "Create Topic" button 

Priority: Low
-----------------------

## TC5
* Test Title: Add tags to new topic

* Precondition: The system is accessible via {webBrowser} and https://forum.telerikacademy.com/ loaded. 
User is logged in.

* Narrative: Аs a student of Telerik Academy I want to add tag while creating a new topic.

* Steps to reproduce:
  1. Click on "New Topic" button
  2. Enter title or paste link in the above area
  3. Press "+" button in optional tags area
  4. Select some tag or tags
  5. Select and Click on "Create Topic" button 

Priority: Medium
-----------------------

## TC6  
* Test Title: Create Topic with name of already existing Topic

* Precondition: The system is accessible via {webBrowser} and https://forum.telerikacademy.com/ loaded. User is logged in.

* Narrative: Аs a student of Telerik Academy I want to create a topic in my cohort's category.

* Steps to reproduce:
  1. Click on "Categories" button from Navigation bar
  2. Click on category name "Alpha 22 QA" hyperlink
  3. Click on "New Topic" button
  4. Enter title or paste link in label area
  5. Enter message in text area
  6. Select and Click on "Create Topic" button 

Priority: Medium
-------------------------

## TC7
* Test Title: Create a topic title with 200 symbols 

* Preconditions:
  1. Access https://forum.telerikacademy.com/ via Google Chrome Web Browser
  2. Log into forum with valid credentials

* Narrative: Try to create a new topic with title length of 200 symbols

* Steps to reproduce:
  1. Click on "New Topic" button on forum main page
  2. Enter 200 symbols as topic title
  3. Enter random text in topic body 
  4. Click on "Create Topic" button below topic body OR press Ctrl + Enter

Priority: Low
----------------------

## TC8
* Test Title: Create new topic with empty title

* Precondition: The system is accessible via webBrowser and https://forum.telerikacademy.com/ loaded. User is logged in.

* Narrative:

* Steps to reproduce:
  1. Click on "+ New Topic" button in the upper rght corner of the Home screen
  2. Enter topic body and leave topic title empty 
  3. Click on "Create Topic" button below topic body OR press Ctrl + Enter

Priority: 
----------------------------

## TC9
* Test Title: Post a reply as registered user

* Precondition: Web browser Google Chrome(Version 85.0.4183.83) is opend and https://forum.telerikacademy.com/ loaded with registerd user.

* Narrative: Аs a student of Telerik Academy I want to to reply to a post/topic and post my answer.

* Steps to reproduce:
  1. Open the path "https://forum.telerikacademy.com/t/git-basics/9166/10"
  2. Scroll down and after the last reply press the blue button "reply".
  3. Enter random text (5 words) in the new popped up text area.
  4. Press the blue "Reply" button.

Priority: Medium
-------------------------------

## TC10
* Test Title: Post a reply as a guest

* Precondition: The system is accessible via {webBrowser} and https://forum.telerikacademy.com/ loaded. 
User is not logged in.

* Narrative: I want to reply to a comment in the forum without being logged in.

* Steps to reproduce:
  1. Select and Click on topic category "Alpha Preparation"
  2. Select and Click on topic "Online course"
  3. Click on the blue button "Reply"


Priority: Low
----------------------------

## TC11
* Test Title: Edit comment as a registered user

* Precondition: The system is accessible via {webBrowser} and https://forum.telerikacademy.com/ loaded. User is logged in.

* Narrative: Аs a student of Telerik Academy I want to edit to my previous topic comment.

* Steps to reproduce:  
1. Click on "Categories" button from Navigation bar
2. Click on category name "Alpha 22 QA" hyperlink
3. Click on topic "Soft Skills - 26.08 - Constructive Feedback" hyperlink
4. Find previous post/comment to edit
5. Click on "Edit this post" button
6. Edit comment in text area
7. Select and Click on "Save Edit" button 

Priority: low
-----------------

## TC12
* Test Title: Remove own topic as a user

* Preconditions:
  1. Access https://forum.telerikacademy.com/ via Google Chrome Web Browser
  2. Log into forum with valid credentials
  3. Create new topic from Home Page (as per steps in TC1 above)

* Narrative: Check if a specific topic created by a user can be deleted by that same user

* Steps to reproduce:
  1. Click on created topic on forum home page
  2. Click on three dots below created topic to open additional actions
  3. Click on thrash bin icon to delete topic

Priority: Medium
------------------

## TC13
* Test Title: Remove post as a registered user

* Precondition: The system is accessible via webBrowser and https://forum.telerikacademy.com/ loaded. User is logged in.

* Narrative: After posting in the forum, a registered user wants to remove it.

* Steps to reproduce:
  1. Go to the post you want to remove
  2. Click on the three dots button ("...") under your post
  3. Click on the trash can button ("delete this post")

Priority: High
-------------------

## TC14
* Test Title: Check if comment contains offensive words

* Precondition: Web browser Google Chrome(Version 85.0.4183.83) is opend and https://forum.telerikacademy.com/ loaded with registerd user.

* Narrative: Аs a registerd user, reply to a topic and try to use some cursing words.

* Steps to reproduce:
  1. Open the path "https://forum.telerikacademy.com/t/git-basics/9166/10"
  2. Scroll down and after the last reply press the blue button "reply".
  3. Enter at least 5 offensive wors - "fuck", "nigger", "dumbass" etc.
  4. Press the blue "Reply" button.

Priority: Low 
-----------------------

## TC15
* Test Title: React to a post as registered user

* Precondition: The system is accessible via {webBrowser} and https://forum.telerikacademy.com/ loaded. 
User is logged in.

* Narrative: Аs a student of Telerik Academy I want to like the post of my co-student.

* Steps to reproduce:
  1. Select and Click on topic category "Alpha 22 QA"
  2. Select and Click on topic "Materials"
  3. Select and Click on the heart button under any of the comments

Priority: Medium
-----------------------

## TC16
* Test Title: Answer to a comment by "Quote"

* Precondition: The system is accessible via {webBrowser} and https://forum.telerikacademy.com/ loaded. User is logged in.

* Narrative: I want to answer to my colleague's topic comment.

* Steps to reproduce:  
  1. Click on "Categories" button from Navigation bar
  2. Click on category name "Alpha 22 QA" hyperlink
  3. Click on topic "Group HTML Page Project" hyperlink
  4. Choose topic comment 
  5. Select the text of the comment
  6. Click on "Quote" 
  7. Enter answer to comment in text area
  8. Select and Click on "Reply" button

Priority: Low
-------------------------

## TC17
* Test Title: Attach image above 4096kb to a reply

* Preconditions:
  1. Access https://forum.telerikacademy.com/ via Google Chrome Web Browser
  2. Log into forum with valid credentials

* Narrative: As a reply to a topic in the forum, try attaching an image that exceeds the maximum allowed size of 4096kb

* Steps to reproduce:
  1. Select "New Topic" from forum home page
  2. Enter random text as title of new topic
  3. In the topic body, click on the "Upload" icon
  4. Select "From my device" radio button
  5. Click on "Choose Files" button 
  6. Browse and select an image larger than 4096kb from your device
  7. Click on "Upload" button

Priority: Low
----------------------

## TC18
* Test Title: Attach file in invalid format in a post

* Precondition: The system is accessible via webBrowser and https://forum.telerikacademy.com/ loaded. User is logged in. 

* Narrative: Registered user wants to upload a file which is not a picture in a post 

* Steps to reproduce:
  1. Go to the topic you want to post in
  2. Click on the "Reply" button
  3. Click on the "Upload" button (7th counting left to right, "picture" pictogram)
  4. Click on "Избор на файлове" button
  5. Select file which is not a picture (file extension is not jpg, jpeg, png, gif, etc.)
  6. Click on the "Upload" button

Priority: Low
----------------------------

## TC19
* Test Title: Insert hyperlink into comment

* Precondition: Web browser Google Chrome(Version 85.0.4183.83) is opend and https://forum.telerikacademy.com/ loaded with registerd user.

* Narrative: s a registerd user, i want to reply to a topic with some external hyperlink.

* Steps to reproduce:
  1. Open the path "https://forum.telerikacademy.com/t/git-basics/9166/10"
  2. Scroll down and after the last reply press the blue button "reply".
  3. Enter message in text area and write a hyperlink "https://en.wikipedia.org/wiki/Dog"
  4. Press the blue "Reply" button.

Priority: Low
-------------------------------

## TC20
* Test Title: Add empty comment

* Precondition: The system is accessible via {webBrowser} and https://forum.telerikacademy.com/ loaded. 
User is logged in.

* Narrative: Аs a student of Telerik Academy I want to create a new comment without any content.

* Steps to reproduce:
  1. Select and Click on topic category "Alpha 22 QA"
  2. Select and Click on topic "Materials"
  3. Hold mouse cursor for hoover and Click on the button "Reply" in the first post from 18th May
  4. Leave the open window for writing a message empty
  5. Select and Click the blue button "Reply"

Priority: Medium
----------------------------
