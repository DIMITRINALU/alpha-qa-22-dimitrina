# Test Plan

* Testing Front-end functionalities from Telerik Academy Forum
* Project URL https://stage-forum.telerikacademy.com/

* Prepared By: Dimitrina
* Date: 23.08.2020

## Introduction
### Project Overview
The Telerik Academy Forum is a online environment to foster collaboration among users for educational purposes and is used as meeting place where programmers come together to discuss their views on different programming techniques, software design methods, testing principle of the software, project management concepts, etc. This is a collaborative learning tool for demonstrating critical thinking and interaction that could lead to better teaching and learning outcomes. It provide additional opportunity for multi-participant interactions to share information outside the classroom discussions in order to bring students and educators a number of benefits.

### Purpose
The purpose of this document is to details the testing that will be performed and to outline the techniques used to assert the software quality according to the main Agile Testing principles. It defines the overall testing requirements and provides an integrated view of the project test activities. 

### Objectives
The general aim of testing applied to Telerik Academy Forum’s topic creation functionality is to provide early feedback and critique to developers about the system under test. The objective of the tests is to verify that the topics creation functionality of Telerik Academy Forum works according to the specifications.The tests will execute and verify the test scripts, identify, fix and retest all high and medium severity defects per the entrance criteria, prioritize lower severity defects for future fixing via Change Request. This hopefully should mitigate the bulk of issues earlier in the release cycle. 

### Scope
The current project is limited to testing the functionality of the Telerik Academy Forum’s topic creation and the functionalities listed under  Functionalities to be testet in the section Scope of testing.  

## Testing leves and types

**Component/Unit testing**
This step is already performed by the development team and is considered done.

**Integration testing**
This step is already performed by the development team, when new modules were developed and added to the system. QA Team may also perform integration testing to ensure that modules integrate without defect issues from users’ perspective. Such testing will be performed using black-box testing method with manual and automated tests. 

**System testing**
This test level will be covered through manual and automated tests and functional, non-functional and regression testing will be performed.

* Functional testing
** User interface testing – will ensure that users are provided with user role access in order to use the functionalities of the modules that are in the scope of testing.
** Validation testing – will ensure that the modules in scope of testing are capable of performing the tasks they are created for. Validation testing will be performed with use cases converted to test cases. 

* Non-functional testing
**	Performance testing – will be done automatically. This type of testing will ensure that the response time of all modules in scope of testing is no more than 30 seconds. The actual time depends on the functionality of each of the modules. More specific criteria will be defined in a separate document containing details for the performance testing.
**	Load Testing – will be done automatically for each module, where high loads are recognized as potential risk.
**	Security Testing – will be done manually/automatically for each module where security risks are recognized. 

* Regression testing – after modification of a component automatic tests will be performed on components related directly or indirectly to the modified one.

**Acceptance testing**
The stakeholders and the project manager have decided that Beta testing would be skipped. Instead the test team should deliver a presentation of all testing activities.

##  Schedule
The test team will meet every day to evaluate progress to date and to identify error trends and problems as early as possible. 
The meetings will be from distance using Skype, if it is necessary. 
There will be one week-long iterations and test design implementation and execution will be performed in each iteration. Each iteration schedule will be discussed and accepted by the team on the weekly sprint planning meeting each Monday at 19:45.

* Test plan preparation: 3 days, from 21.08.2020 to 23.08.2020
* Test plan presentation to stakeholders: 1 day, 23.08.2020	
* Fulfill training and environmental needs: 3 days, from 24.08.2020 to 26.08.2020	
* Test cases preparation: 5 days, from 24.08.2020 to 28.08.2020		
* Test scripts preparation: 4 days, from 01.09.2020 to 04.09.2020	
* Smoke testing: It will be specified in the testing process				
* System testing: It will be specified in the testing process				
* Load testing: It will be specified in the testing process				
* Performance testing: It will be specified in the testing process				
* Security testing: It will be specified in the testing process				
* Test closure activities: It will be specified
* ...

## Scope of testing
This section details functionalities to be tested and functionalities out of scope of testing

### Functionalities to be tested
* Log in
* Registration
* Topic creation
* Reply on topic
* Share link to topic
* 'Like' button on topic
* 'Bookmark' button on topic
* 'Flag' button on topic
* ...

### Functionalities not to be tested
  Navigation bar:
* User Settings
* Messages
* Panels for Latest Forum'topics Posts
* Panels for Unread'topics Posts
* Panels for Top'topics Posts
* FAQ section
* Search
* Learning Platform 
* ...

## Entry criteria  
* Unit testing performed for each module by the development team and should cover more than 95 % of the newly developed code
* Integration testing performed for all modules under test
* The testing will begin when there is testing environment available at https://stage-forum.telerikacademy.com/, set up and functional
* Design documentation and requirements should be available so the testing team can verify their test
* Hardware and software with the requested tools must be provided to the testing team 
* All team members must have completed the necessary training
* ...

## Exit Criteria
* 100% statement coverage
* 100% requirement coverage
* 100% of test cases have been run
* 100% of critical and high severity faults fixed
* 80% of low & medium severity faults fixed
* All critical and high risk areas are tested thoroughly  
* Maximum of 5 known faults remain
* System continues to function without blocking defects under anticipated and realistic loads 
* .... 