package com.telerikacademy;

import java.util.*;

public class Main {

    public static void main(String[] args) {
	    //test LinkedList
//        testLinkedList();
        //test Stack
//        testStack();
        //test HashMap
        testHashMap();
        //test Queue
//        testQueue();
    }

    private static void testQueue() {
        //Test queue methods: offer(); peek(); poll(); isEmpty();
        //notice that we are implementing queue via LinkedList and therefore we have access to its methods -> however a cast is required in order to use them!
        Queue<Double> queue = new LinkedList<>();
        queue.offer(4.5);
        queue.offer(5.6);
        queue.offer(6.7);
        System.out.printf("Queue first element: %.1f and queue last element: %.1f!\n",
                queue.peek(),
                ((LinkedList<Double>) queue).peekLast());

        double lastElement = 0;
        while (!queue.isEmpty()) {
            lastElement = queue.poll();
        }
        System.out.printf("Queue last element: %.1f", lastElement);
    }

    private static void testHashMap() {
        //Test Map methods: put(key, value); get(key); getOrDefault(key, default value); keySet();
        // !-> https://www.journaldev.com/11641/java-map
        HashMap<String, Integer> map = new HashMap<>();
        map.put("pesho", 32);
        map.put("gosho", 43);
        map.put("tosho", 33);
        System.out.printf("Pesho, Gosho and Tosho are: %d, %d and %d years old\n",
                map.get("pesho"),
                map.get("gosho"),
                map.get("tosho"));

        System.out.printf("Everyone else is considered %d years old\n",
                map.getOrDefault("SomeRandomKey", 28));

        for (String name : map.keySet()) {
            System.out.println(name + map.get(name));
        }



        map.forEach((name,age)-> System.out.printf("%s - %d!\n\n", name,age) );

        testEntrySet(map);
    }

    private static void testEntrySet(HashMap<String, Integer> map) {
        //Extra - additional  ways to interact with Map

        //Entry has both getKey() and getValue() methods
//        for (Map.Entry<String, Integer> entry : map.entrySet())
//        {
//            String key = entry.getKey();
//            int value = entry.getValue();
//            System.out.println(key + " - " + value);
//        }
//        System.out.println();

        //Above we are actually creating unique Set of Entry (Key, Value)
//        Set<Map.Entry<String, Integer>> entrySet = map.entrySet();
//        entrySet.forEach(eSet -> System.out.println(eSet.getKey() + " !!! " + eSet.getValue()));
        //this is how it looks when we print the set directly:
//        System.out.println(entrySet);
    }

    private static void testStack() {
        //Test Stack methods: push(); peek(); pop(); isEmpty()
        Stack<String> stack = new Stack<>();
        stack.push("First added");
        stack.push("Second added");
        stack.push("Third added");
        for (int i = 0; i < stack.size(); i++) {
            System.out.printf("Stack peek: %s; ", stack.peek());
        }
        System.out.println();

        while (!stack.isEmpty()) {
            System.out.println("Stack pop element: " + stack.pop());
        }
    }

    private static void testLinkedList() {
        //Test LinkedList methods: add(); get(); size(); pollFirst(); pollLast(); peekFirst(); peekLast();
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.add(1);
        linkedList.add(3);
        System.out.println(linkedList.pollFirst() + linkedList.peekLast());
        System.out.println("LinkedList size is: " + linkedList.size());
    }
}
