import com.telerikacademy.QueueImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class QueueTests {

    private QueueImpl<String> queue;

    @BeforeEach
    public void setUp() {
        this.queue = new QueueImpl<>();
    }

    @Test(expected = NullPointerException.class)
    public void Constructor_ShouldThrow_WhenMakeIsNull() {
        new QueueImpl<>(null);
    }

    @Test
    public void constructorWithParameterShouldAddElementsCorrectly() {
        ArrayList<String> myList = new ArrayList<>();
        myList.add("Slayer");
        myList.add("WASP");

        QueueImpl<String> queue = new QueueImpl<>(myList);

        assertEquals("Slayer", queue.peek());
    }

    @Test
    public void testOfferShouldIncreaseSize() {
        this.queue = new QueueImpl<>();
        for (int i = 0; i < 100; i++) {
            queue.offer(String.valueOf(i));
        }
        assertEquals(100, queue.size());
    }

    @Test
    public void testOfferShouldAddAtTheEnd() {
        this.queue = new QueueImpl<>();
        for (int i = 0; i < 100; i++) {
            queue.offer(String.valueOf(i));
        }
        queue.offer("Slayer");
        assertEquals("0", queue.peek());
    }

    @Test
    public void testPopShouldRemoveAndReturnTopElement() {
        this.queue = new QueueImpl<>();
        for (int i = 0; i < 100; i++) {
            queue.offer(String.valueOf(i));
        }
        assertEquals("0", queue.poll());
        assertEquals(99, queue.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPopShouldThrowWhenEmpty() {
        new QueueImpl<>().poll();
    }

    @Test
    public void testPeekShouldReturnAndNotRemove() {
        this.queue = new QueueImpl<>();
        for (int i = 0; i < 100; i++) {
            queue.offer(String.valueOf(i));
        }
        assertEquals("0", queue.peek());
        assertEquals(100, queue.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPeekShouldThrowWhenEmpty() {
        new QueueImpl<>().peek();
    }

    @Test
    public void testSizeShouldReturnCorrectNumberOfElements() {
        this.queue = new QueueImpl<>();
        for (int i = 0; i < 100; i++) {
            queue.offer(String.valueOf(i));
        }
        assertEquals(100, queue.size());
        assertEquals(0, new QueueImpl<>().size());
    }

    @Test
    public void testIsEmpty() {
        this.queue = new QueueImpl<>();
        for (int i = 0; i < 100; i++) {
            queue.offer(String.valueOf(i));
        }
        assertTrue(new QueueImpl<>().isEmpty());
        assertFalse(queue.isEmpty());
    }
}