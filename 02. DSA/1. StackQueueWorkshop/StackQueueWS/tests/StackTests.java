import com.telerikacademy.StackImpl;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class StackTests {

    private StackImpl<String> stack;

    @BeforeEach
    public void setUp() {
        this.stack = new StackImpl<>();
    }

    @Test(expected = NullPointerException.class)
    public void Constructor_ShouldThrow_WhenIsNull() {
        new StackImpl<>(null);
    }

    @Test
    public void constructorWithParameterShouldAddElementsCorrectly() {
        ArrayList<String> myList = new ArrayList<>();
        myList.add("Slayer");
        myList.add("WASP");

        StackImpl<String> stack = new StackImpl<>(myList);

        assertEquals("WASP", stack.peek());
    }

    @Test
    public void testPushShouldIncreaseSize() {
        this.stack = new StackImpl<>();
        for (int i = 0; i < 100; i++) {
            stack.push(String.valueOf(i));
        }
        assertEquals(100, stack.size());
    }

    @Test
    public void testPushShouldAddAtTheTop() {
        this.stack = new StackImpl<>();
        stack.push("Slayer");
        assertEquals("Slayer", stack.peek());
    }

    @Test
    public void testPopShouldRemoveAndReturnTopElement() {
        this.stack = new StackImpl<>();
        for (int i = 0; i < 100; i++) {
            stack.push(String.valueOf(i));
        }
        assertEquals("99", stack.pop());
        assertEquals(99, stack.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPopShouldThrowWhenEmpty() {
        new StackImpl<>().pop();
    }

    @Test
    public void testPeekShouldReturnAndNotRemove() {
        this.stack = new StackImpl<>();
        for (int i = 0; i < 100; i++) {
            stack.push(String.valueOf(i));
        }
        assertEquals("99", stack.peek());
        assertEquals(100, stack.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPeekShouldThrowWhenEmpty() {
        new StackImpl<>().peek();
    }

    @Test
    public void testSizeShouldReturnCorrectNumberOfElements() {
        this.stack = new StackImpl<>();
        for (int i = 0; i < 100; i++) {
            stack.push(String.valueOf(i));
        }
        assertEquals(0, new StackImpl<>().size());
        assertEquals(100, stack.size());
    }

    @Test
    public void testIsEmpty() {
        this.stack = new StackImpl<>();
        for (int i = 0; i < 100; i++) {
            stack.push(String.valueOf(i));
        }
        assertTrue(new StackImpl<>().isEmpty());
        assertFalse(stack.isEmpty());
    }
}