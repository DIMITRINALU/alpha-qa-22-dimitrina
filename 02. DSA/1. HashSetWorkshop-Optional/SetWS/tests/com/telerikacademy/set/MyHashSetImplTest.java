package com.telerikacademy.set;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.*;

public class MyHashSetImplTest {

    @BeforeEach
    public void before() {

        // Default initial capacity is 16
        // Default load factor is 0.75

        MyHashSetImpl<String> set = new MyHashSetImpl<>();
    }

    @Test
    public void add_OnEmptyHashSet_NoDuplicates_ShouldAddElement() {

        MyHashSetImpl<ArrayList<String>> set = new MyHashSetImpl<>();

        ArrayList<String>[] buckets = new ArrayList[10];

        for (ArrayList<String> bucket : buckets) {
            set.add(bucket);
        }

        for (ArrayList<String> bucket : buckets) {
            Assert.assertTrue(set.contains(bucket));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void remove_NonExistingElement_ShouldWorkCorrectly() {

        MyHashSetImpl<String> set = new MyHashSetImpl<>();
        assertEquals(0, set.size());

        set.add("Peter");
        set.add("Maria");

        Assert.assertEquals(2, set.size());

        set.remove("George");

        Assert.assertFalse(true);
        Assert.assertEquals(2, set.size());
    }

    @Test
    public void remove_5000_Elements_ShouldWorkCorrectly() {

        MyHashSetImpl<String> set = new MyHashSetImpl<>();
        ArrayList<String> buckets = new ArrayList<>();
        int count = 5000;

        for (int i = 0; i < count; i++) {
            buckets.add(String.valueOf(i));
            set.add(String.valueOf(i));
        }

        Assert.assertEquals(count, set.size());

        for (String bucket : buckets) {
            set.remove(bucket);
            count--;
            Assert.assertEquals(count, set.size());
        }
    }

    @Test
    public void size() {

        MyHashSetImpl<String> set = new MyHashSetImpl<>();
        set.add("Ivan");

        assertEquals(1, set.size());
    }

    @Test
    public void checkingForElement_shouldReturnTrueOrFalseIfElementContains() {

        MyHashSetImpl<String> set = new MyHashSetImpl<>();
        set.add("Ivan");

        assertTrue("Returned false for valid object", set.contains("Ivan"));
        assertTrue("Returned true for invalid Object", !set
                .contains("George"));

        MyHashSetImpl<String> setTwo = new MyHashSetImpl<>();
        setTwo.add(null);

        assertTrue("Cannot handle null", setTwo.contains(null));
    }

    @Test
    public void capacity() {

        MyHashSetImpl<String> set = new MyHashSetImpl<>();

        Assert.assertEquals(16, set.capacity());

        set.add("Stan");
        set.add("Maria");

        Assert.assertEquals(16, set.capacity());

        set.add("Tanya");

        assertEquals(16, set.capacity());
    }
}