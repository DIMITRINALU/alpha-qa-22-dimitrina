import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class TelerikAcademyForumTests {

    private static final String GOOGLE_URL = "https://www.google.com/";
    private static final String TELERIK_URL = "https://stage-forum.telerikacademy.com/";

    private static WebDriver driver;
    WebElement webElement;

    @BeforeClass
    public static void classInitialize() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\ChromeDriver\\chromedriver.exe");
        //System.setProperty("webdriver.gecko.driver", "C:\\Users\\murto\\Desktop\\GeckoDriver\\geckodriver.exe");
    }

    @Before
    public void openBrowser() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        driver.get(GOOGLE_URL);
    }

    @Before
    public void logInForum() {
        acceptGoogleConsent();
        driver.get(TELERIK_URL);
        Assert.assertEquals(driver.getCurrentUrl(), TELERIK_URL);
        driver.findElement(By.className("d-button-label")).click();
        driver.findElement(By.id("Email")).sendKeys("dimitrinamurtova@gmail.com");
        driver.findElement(By.id("Password")).sendKeys("test");
        driver.findElement(By.id("next")).click();

        try {
            webElement = driver.findElement(By.className("d-button-label"));
        } catch (Exception e) {
            Assert.assertNotNull(webElement);
        }
    }

    @Test
    public void createNewTopic_When_ForumHomePageAppears() {
        driver.findElement(By.id("create-topic")).click();
        driver.findElement(By.id("reply-title")).sendKeys("just test");
        driver.findElement(By.xpath("//textarea[@aria-label='Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images.']")).sendKeys("this is my new test topic");
        driver.findElement(By.xpath("//button[@aria-label='Create Topic']")).click();

        String expected = "just test";
        Assert.assertTrue("Error! Current URL is:" + driver.getCurrentUrl() + expected, driver.getCurrentUrl().contains(TELERIK_URL));
    }

    @Test
    public void postReply_When_YouWantToCommentOnTheTopic() {
        findTopic("https://stage-forum.telerikacademy.com/t/just-test/8279");
        driver.findElement(By.xpath("//button[@aria-label='Reply']")).click();
        driver.findElement(By.xpath("//textarea")).sendKeys("Great!");
        driver.findElement(By.xpath("(//span[contains(.,'Reply')])[last()]")).click();

        WebElement replyExpected = driver.findElement(By.xpath("//*[@id='post_2']//p"));
        Assert.assertTrue("The reply is displayed!", replyExpected.isDisplayed());
    }

    @Test
    public void editPost_When_AlreadyTopicIsCreated() {
        findTopic("https://stage-forum.telerikacademy.com/t/just-test/8279");
        driver.findElement(By.xpath("//button[@aria-label='edit this post']")).click();
        driver.findElement(By.xpath("//textarea")).sendKeys(". Just great!");
        driver.findElement(By.xpath("(//span[contains(.,'Save Edit')])[last()]")).click();

        WebElement textExpected = driver.findElement(By.xpath("//*[@id='post_1']//p"));
        Assert.assertTrue("The text is expected and everything seems good!", textExpected.isDisplayed());
    }

    @Test
    public void reactToPost_When_YouLikeTheTopic() {
        findTopic("https://stage-forum.telerikacademy.com/t/selenium-life-is-better-than-no-life-at-all/8070");
        driver.findElement(By.xpath("(//button[@aria-label='like this post'])")).click();

        WebElement expected = driver.findElement(By.xpath("(//button[@aria-label='like this post'])"));
        WebElement actual = driver.findElement(By.xpath("(//button[@aria-label='undo like'])"));
        Assert.assertNotSame("You've  already liked this post!:", expected, actual);
    }

    @Test
    public void removePost_When_TheTopicIsNoLongerRelevant() {
        findTopic("https://stage-forum.telerikacademy.com/t/just-test/8278");
        driver.findElement(By.xpath("(//button[@aria-label='show more'])[last()]")).click();
        driver.findElement(By.xpath("(//button[@aria-label='delete topic'])[last()]")).click();

        Assert.assertTrue("(topic withdrawn by author, will be automatically deleted in 24 hours unless flagged)", true);
    }

    private void findTopic(String expected) {
        driver.get(expected);
        Assert.assertNotSame(expected, driver.getCurrentUrl());
    }

    private void acceptGoogleConsent() {
        webElement = driver.findElement(By.xpath("//div[@id='cnsw']/iframe"));
        driver.switchTo().frame(webElement);
        driver.findElement(By.id("introAgreeButton")).click();
        driver.switchTo().defaultContent();
    }

    @After
    public void closeBrowser() {
        driver.quit();
    }
}