package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class GooglePage extends BasePage {

    public GooglePage(WebDriver driver) {
        super(driver);
    }

    public void SearchAndOpenFirstResult(String searchTerm) {
        acceptGoogleConsent();
        actions.waitForElementVisible("search.Input", 50);
        actions.assertElementPresent("search.Input");
        actions.typeValueInField(searchTerm, "search.Input");
        actions.waitForElementVisible("search.Button", 50);
        actions.clickElement("search.Button");
        actions.waitForElementVisible("search.Result", 50);
        actions.clickElement("search.Result");
    }

    public void acceptGoogleConsent() {
        actions.waitForElementVisible("consent.Iframe", 50);
        actions.changeIFrame("consent.Iframe");
        actions.waitForElementVisible("consent.Agree", 50);
        actions.clickElement("consent.Agree");
        Utils.getWebDriver().switchTo().defaultContent();
    }
}