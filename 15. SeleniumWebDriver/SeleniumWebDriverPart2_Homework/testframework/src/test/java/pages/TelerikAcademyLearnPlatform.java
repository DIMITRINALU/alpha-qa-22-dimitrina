package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class TelerikAcademyLearnPlatform extends BasePage {

    final String PAGE_URL = Utils.getConfigPropertyByKey("learn.url");

    public TelerikAcademyLearnPlatform(WebDriver driver) {
        super(driver);
    }

    public void openTelerikAcademyLearnPlatform() {
        Utils.getWebDriver().get(PAGE_URL);
    }

    public void LogInLearnTelerikAcademy() {
        actions.waitForElementVisible("learn.LogIn", 150);
        actions.assertElementPresent("learn.LogIn");
        actions.clickElement("learn.LogIn");
        actions.waitForElementVisible("login.Email", 50);
        actions.clickElement("login.Email");
        actions.typeValueInField("dimitrinamurtova@gmail.com", "login.Email");
        actions.clickElement("login.Password");
        actions.typeValueInField("test", "login.Password");
        actions.clickElement("login.SignIn");
    }

    public void AssertAvatarPresent() {
        actions.assertElementPresent("login.Avatar");
    }
}
