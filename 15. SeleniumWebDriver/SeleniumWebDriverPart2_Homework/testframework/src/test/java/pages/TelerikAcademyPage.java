package pages;

import org.openqa.selenium.WebDriver;

public class TelerikAcademyPage extends BasePage {

    public TelerikAcademyPage(WebDriver driver) {
        super(driver);
    }

    public void NavigateToQACourseViaCard() {
        actions.clickElement("academy.AlphaAnchor");
        actions.clickElement("academy.QaGetReadyLink");
        actions.clickElement("academy.SignUpNavButton");
    }

    public void AssertQAAcademySignupPageNavigated() {
        actions.assertNavigatedUrl("academy.QASignUpUrl");
    }
}