package pages;

import com.telerikacademy.testframework.UserActions;
import org.openqa.selenium.WebDriver;

public abstract class BasePage {

    private WebDriver driver;
    private String Url;
    public UserActions actions;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        actions = new UserActions();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        this.Url = url;
    }
}