package testCases;

import org.junit.Test;
import pages.GooglePage;

public class SearchInGoogleTest extends BaseTest {

    String searchCriterion = "Telerik Academy";

    @Test
    public void simpleGoogleSearch() {
        GooglePage google = new GooglePage(actions.getDriver());
        google.SearchAndOpenFirstResult(searchCriterion);
        navigateToQACourseViaCard();
        actions.assertNavigatedUrl("academy.QASignUpUrl");
    }

    private void navigateToQACourseViaCard() {
        actions.clickElement("academy.AlphaAnchor");
        actions.clickElement("academy.QaGetReadyLink");
        actions.clickElement("academy.SignUpNavButton");
    }
}