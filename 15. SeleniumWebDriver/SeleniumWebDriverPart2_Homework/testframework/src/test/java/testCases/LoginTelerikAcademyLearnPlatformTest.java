package testCases;

import com.telerikacademy.testframework.Utils;
import org.junit.Test;
import pages.GooglePage;
import pages.TelerikAcademyLearnPlatform;

import java.util.concurrent.TimeUnit;

public class LoginTelerikAcademyLearnPlatformTest extends BaseTest {

    @Test
    public void logInLearnAcademy() {
        GooglePage google = new GooglePage(actions.getDriver());
        google.acceptGoogleConsent();
        TelerikAcademyLearnPlatform learn = new TelerikAcademyLearnPlatform(actions.getDriver());
        learn.openTelerikAcademyLearnPlatform();
        learn.LogInLearnTelerikAcademy();
        Utils.getWebDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        learn.AssertAvatarPresent();
    }
}