package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.Role;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.User;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.dealership.models.ModelsConstants.*;

public class UserImpl implements User {
    
    private final static String USERNAME_FIELD = "Username must be between 2 and 20 characters long!";
    private final static String FIRST_NAME_FIELD = "Firstname must be between 2 and 20 characters long!";
    private final static String LAST_NAME_FIELD = "Lastname must be between 2 and 20 characters long!";
    private final static String PASSWORD_FIELD = "Password must be between 5 and 30 characters long!";
    private static final String ROLE_CANT_BE_NULL = "Role cannot be null";
    private final static String NO_VEHICLES_HEADER = "--NO VEHICLES--";
    private final static String USER_HEADER = "--USER %s--";

    private String username;
    private String firstName;
    private String lastName;
    private String password;
    private Role role;
    private final List<Vehicle> vehicles;
    
    public UserImpl(String username, String firstName, String lastName, String password, Role role) {
        setUsername(username);
        setFirstName(firstName);
        setLastName(lastName);
        setPassword(password);
        setRole(role);
        validateState();

        vehicles = new ArrayList<>();
    }

    private void setUsername(String username) {
        Validator.ValidateIntRange(username.length(),MIN_NAME_LENGTH , MAX_NAME_LENGTH, USERNAME_FIELD);
        Validator.ValidateSymbols(username, USERNAME_PATTERN, "Username contains invalid symbols!");
        this.username = username;
    }

    private void setFirstName(String firstName) {
        Validator.ValidateIntRange(firstName.length(),MIN_NAME_LENGTH , MAX_NAME_LENGTH, FIRST_NAME_FIELD);
        this.firstName = firstName;
    }

    private void setLastName(String lastName) {
        Validator.ValidateIntRange(lastName.length(),MIN_NAME_LENGTH , MAX_NAME_LENGTH, LAST_NAME_FIELD);
        this.lastName = lastName;
    }

    private void setPassword(String password) {
        Validator.ValidateIntRange(password.length(),MIN_PASSWORD_LENGTH , MAX_PASSWORD_LENGTH, PASSWORD_FIELD);
        Validator.ValidateSymbols(password, PASSWORD_PATTERN, "Password contains invalid symbols!");
        this.password = password;
    }

    private void setRole(Role role) {
        Validator.ValidateNull(role, ROLE_CANT_BE_NULL);
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    public List<Vehicle> getVehicles() {
        return new ArrayList<>(vehicles);
    }
    
    public void addComment(Comment commentToAdd, Vehicle vehicleToAddComment) {
        
        Validator.ValidateNull(commentToAdd, ModelsConstants.COMMENT_CANNOT_BE_NULL);
        Validator.ValidateNull(vehicleToAddComment, ModelsConstants.COMMENT_CANNOT_BE_NULL);
        
        vehicleToAddComment.addComment(commentToAdd);
    }
    
    public void addVehicle(Vehicle vehicle) {
        
        Validator.ValidateNull(vehicle, ModelsConstants.VEHICLE_CANNOT_BE_NULL);
        if (role == Role.NORMAL && vehicles.size() >= 5) {
            
            throw new IllegalArgumentException(
                    String.format(ModelsConstants.NOT_AN_VIP_USER_VEHICLES_ADD, ModelsConstants.MAX_VEHICLES_TO_ADD));
        }
        
        if (role == Role.ADMIN) {
            throw new IllegalArgumentException(ModelsConstants.ADMIN_CANNOT_ADD_VEHICLES);
        }
        
        vehicles.add(vehicle);
    }
    
    public void removeComment(Comment commentToRemove, Vehicle vehicleToRemoveComment) {
        
        Validator.ValidateNull(vehicleToRemoveComment, ModelsConstants.VEHICLE_CANNOT_BE_NULL);
        Validator.ValidateNull(commentToRemove, ModelsConstants.COMMENT_CANNOT_BE_NULL);
        
        if (!username.equals(commentToRemove.getAuthor())) {
            throw new IllegalArgumentException(ModelsConstants.YOU_ARE_NOT_THE_AUTHOR);
        }
        vehicleToRemoveComment.removeComment(commentToRemove);
    }
    
    public void removeVehicle(Vehicle vehicle) {
        
        Validator.ValidateNull(vehicle, ModelsConstants.VEHICLE_CANNOT_BE_NULL);
        
        vehicles.remove(vehicle);
    }
    
    public String toString() {
        
        return String.format(ModelsConstants.USER_TO_STRING, username, firstName, lastName, role);
    }
    
    public String printVehicles() {
        
        StringBuilder builder = new StringBuilder();
        
        int counter = 1;
        builder.append(String.format(USER_HEADER, username)).append(System.lineSeparator());
        
        if (vehicles.size() <= 0) {
            builder.append(NO_VEHICLES_HEADER).append(System.lineSeparator());
        } else {
            for (Vehicle vehicle : vehicles) {
                
                builder.append(String.format("%d. %s", counter, vehicle.toString())).append(System.lineSeparator());
                counter++;
            }
        }
        
        return builder.toString().trim();
    }
    
    private void validateState() {
        
        Validator.ValidateNull(username, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL, USERNAME_FIELD));
        
        Validator.ValidateSymbols(username, ModelsConstants.USERNAME_PATTERN,
                String.format(ModelsConstants.INVALID_SYMBOLS, USERNAME_FIELD));
        
        Validator.ValidateIntRange(username.length(),
                ModelsConstants.MIN_NAME_LENGTH, ModelsConstants.MAX_NAME_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,
                        USERNAME_FIELD, ModelsConstants.MIN_NAME_LENGTH, ModelsConstants.MAX_NAME_LENGTH));
        
        Validator.ValidateNull(firstName, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL, FIRST_NAME_FIELD));
        
        Validator.ValidateIntRange(firstName.length(),
                ModelsConstants.MIN_NAME_LENGTH, ModelsConstants.MAX_NAME_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,
                        FIRST_NAME_FIELD, ModelsConstants.MIN_NAME_LENGTH, ModelsConstants.MAX_NAME_LENGTH));
        
        Validator.ValidateNull(lastName, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL, LAST_NAME_FIELD));
        
        Validator.ValidateIntRange(lastName.length(),
                ModelsConstants.MIN_NAME_LENGTH, ModelsConstants.MAX_NAME_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,
                        LAST_NAME_FIELD, ModelsConstants.MIN_NAME_LENGTH, ModelsConstants.MAX_NAME_LENGTH));
        
        Validator.ValidateNull(password, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL, PASSWORD_FIELD));
        
        Validator.ValidateSymbols(password, ModelsConstants.PASSWORD_PATTERN,
                String.format(ModelsConstants.INVALID_SYMBOLS, PASSWORD_FIELD));
        
        Validator.ValidateIntRange(password.length(),
                MIN_PASSWORD_LENGTH, ModelsConstants.MAX_PASSWORD_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,
                        PASSWORD_FIELD, MIN_PASSWORD_LENGTH, ModelsConstants.MAX_PASSWORD_LENGTH));
        
    }
}