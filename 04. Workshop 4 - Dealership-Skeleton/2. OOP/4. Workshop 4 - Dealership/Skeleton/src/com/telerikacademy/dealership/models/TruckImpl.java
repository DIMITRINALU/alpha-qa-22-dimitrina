package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;

import static com.telerikacademy.dealership.models.ModelsConstants.MAX_CAPACITY;
import static com.telerikacademy.dealership.models.ModelsConstants.MIN_CAPACITY;

public class TruckImpl extends  VehicleBase implements Truck {

    private static final String WEIGHT_MSG = "Weight capacity must be between 1 and 100!";
    private static final int WHEEL_COUNT = 8;

    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
                super(make, model, price, VehicleType.TRUCK);
                setWeightCapacity(weightCapacity);
    }

    private void setWeightCapacity(int weightCapacity) {
        Validator.ValidateIntRange(weightCapacity, MIN_CAPACITY, MAX_CAPACITY, WEIGHT_MSG);
        this.weightCapacity = weightCapacity;
    }

    @Override
    protected String printAdditionalInfo()
    {
        return String.format("  Weight Capacity: %dt", weightCapacity);
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    @Override
    public int getWheels() {
        return WHEEL_COUNT;
    }
}