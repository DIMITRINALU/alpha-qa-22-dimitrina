package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;

import static com.telerikacademy.dealership.models.ModelsConstants.MAX_SEATS;
import static com.telerikacademy.dealership.models.ModelsConstants.MIN_SEATS;

public class CarImpl extends VehicleBase implements Car {

    private static final String SEATS_MSG = "Seats must be between 1 and 10!";
    private static final int WHEEL_COUNT = 4;

    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, VehicleType.CAR);
        setSeats(seats);
    }

    private void setSeats(int seats) {
        Validator.ValidateIntRange(seats, MIN_SEATS, MAX_SEATS, SEATS_MSG);
        this.seats = seats;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Seats: %d", seats);
    }

    @Override
    public int getSeats() {
        return seats;
    }

    @Override
    public int getWheels() {
        return WHEEL_COUNT;
    }
    
}