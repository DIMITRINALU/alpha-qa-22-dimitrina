package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.dealership.commands.constants.CommandConstants.COMMENT_DOES_NOT_EXIST;
import static com.telerikacademy.dealership.commands.constants.CommandConstants.REMOVED_COMMENT_DOES_NOT_EXIST;
import static com.telerikacademy.dealership.models.ModelsConstants.*;

public abstract class VehicleBase implements Vehicle {
    
    private final static String MAKE_FIELD = "Make must be between 2 and 15 characters long!";
    private final static String MODEL_FIELD = "Model must be between 1 and 15 characters long!";
    private final static String PRICE_FIELD = "Price must be between 0.0 and 1000000.0!";
    private final static String WHEELS_FIELD = String.format("Wheels must be between %s and %s!", MIN_WHEELS, MAX_WHEELS);
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";
    private static final String TYPE_CANT_BE_NULL = "Type cannot be null";

    private String make;
    private String model;
    private double price;
    private VehicleType vehicleType;
    private final List<Comment> comments;

    VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        setMake(make);
        setModel(model);
        setPrice(price);
        setVehicleType(vehicleType);

        this.comments = new ArrayList<>();
    }

    protected void setMake(String make) {
        Validator.ValidateIntRange(make.length(), MIN_MAKE_LENGTH, MAX_MAKE_LENGTH, MAKE_FIELD);
        this.make = make;
    }

    @Override
    public String getMake() {
        return make;
    }

    private void setModel(String model) {
        Validator.ValidateIntRange(model.length(), MIN_MODEL_LENGTH, MAX_MODEL_LENGTH, MODEL_FIELD);
        this.model = model;
    }

    @Override
    public String getModel() {
        return model;
    }

    protected void setPrice(double price) {
        Validator.ValidateDecimalRange(price, MIN_PRICE, MAX_PRICE, PRICE_FIELD);
        this.price = price;
    }

    @Override
    public double getPrice() {
        return price;
    }

    protected void setVehicleType(VehicleType vehicleType) {
        Validator.ValidateNull(vehicleType, TYPE_CANT_BE_NULL);
        this.vehicleType = vehicleType;
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    int wheels = 0;

    @Override
    public int getWheels()
    {
        if (wheels < MIN_WHEELS || wheels > MAX_WHEELS)
        {
           throw new IllegalArgumentException(WHEELS_FIELD);
        }
        return wheels;
    }

    @Override
    public void removeComment(Comment comment) {

        if (comment == null) {

            throw new IllegalArgumentException(COMMENT_DOES_NOT_EXIST);
        }

        if (!comments.contains(comment)){
            throw new IllegalArgumentException(REMOVED_COMMENT_DOES_NOT_EXIST);
        }
        this.comments.remove(comment);
    }

    @Override
    public void addComment(Comment comment) {

        if (comment == null) {

            throw new IllegalArgumentException();
        }
        this.comments.add(comment);
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());

        builder.append(String.format("  Make: %s", getMake())).append(System.lineSeparator());
        builder.append(String.format("  Model: %s", getModel())).append(System.lineSeparator());
        builder.append(String.format("  Wheels: %d", getWheels())).append(System.lineSeparator());
        builder.append(String.format("  Price: $%s", Utils.removeTrailingZerosFromDouble(getPrice()))).append(System.lineSeparator());
        
        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }

    protected abstract String printAdditionalInfo();
    
    private String printComments() {
        StringBuilder builder = new StringBuilder();
        
        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());
            
            for (Comment comment : comments) {
                builder.append(comment.toString());
            }
            
            builder.append(String.format("%s", COMMENTS_HEADER));
        }
        
        return builder.toString();
    }
}