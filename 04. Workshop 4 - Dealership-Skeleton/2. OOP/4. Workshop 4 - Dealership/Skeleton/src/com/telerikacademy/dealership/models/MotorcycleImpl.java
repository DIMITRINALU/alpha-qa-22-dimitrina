package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

import static com.telerikacademy.dealership.models.ModelsConstants.MAX_CATEGORY_LENGTH;
import static com.telerikacademy.dealership.models.ModelsConstants.MIN_CATEGORY_LENGTH;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    private static final String CATEGORY_LENGTH_MSG = "Category must be between 3 and 10 characters long!";
    private static final int WHEEL_COUNT = 2;

    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE);
        setCategory(category);
    }

    private void setCategory(String category) {
        Validator.ValidateIntRange(category.length(), MIN_CATEGORY_LENGTH, MAX_CATEGORY_LENGTH, CATEGORY_LENGTH_MSG);
        this.category = category;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Category: %s", category);
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public int getWheels() {
        return WHEEL_COUNT;
    }
}